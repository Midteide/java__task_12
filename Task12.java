
public class Task12 {

    // Added for easy use of colors
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    // Function for checking if string is a valid number
    public static boolean isNumeric(String str) {
        try {
          Double.parseDouble(str);
          return true;
        } catch(NumberFormatException e){
          return false;
        }
      }

    public static void printInstructions() {
        System.out.println("Please provide arguments in following format: 'java Task12.java <width> <height>'");
        System.out.println("Example: 'java Task12.java 15 23'");

      }public static void main( String[] args ) {

        // Check if both arguments are provided
        if (args.length < 2) {
            printInstructions();
            System.exit(0) ;
        }
        // Check if width argument is a valid number
        else if (!isNumeric(args[0])) {
            System.out.println("Please enter a valid width");
            printInstructions();
            System.exit(0) ;
        }
        // Check if height argument is a valid number
        else if (!isNumeric(args[1])) {
            System.out.println("Please enter a valid height");
            printInstructions();
            System.exit(0) ;
        }
        int width = Integer.parseInt(args[0]);
        int height = Integer.parseInt(args[1]);
        
        for (int i=0; i < height ; i++ ) {
            System.out.printf(ANSI_RED  + "#" + ANSI_RESET); // Print outer left border
            for (int j=1; j < width-1 ; j++ ) {
                if ((i == 0) || (i == height-1) ) System.out.printf(ANSI_BLUE + "#" + ANSI_RESET); // Print outer top and bottom border
                else if (((i == 2) || (i == height-3) ) && ((j > 2) && (j < width-3)) ) System.out.printf(ANSI_PURPLE + "$" + ANSI_RESET); // Print inner upper and bottom border
                else if ( ((i > 1) && (i < height-2)) && ((j == 2) || (j == width-3)) )  System.out.printf(ANSI_CYAN + "$" + ANSI_RESET); // Print inner left and right border
                else System.out.printf(" " + ANSI_RESET); // Fill up with space
            }
            System.out.println(ANSI_GREEN + "#" + ANSI_RESET); // Print outer right border
        }
    }
}